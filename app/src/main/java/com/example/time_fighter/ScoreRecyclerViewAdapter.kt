package com.example.time_fighter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.FirebaseFirestore

class ScoreRecyclerViewAdapter : RecyclerView.Adapter<scoreViewHolder>() {

    val db = FirebaseFirestore.getInstance()
    val players: MutableList<Player> = mutableListOf<Player>()

    init {
        val playersRef = db.collection("players")
        playersRef
            .orderBy("score")
            .addSnapshotListener { snapshot, error ->
            if (error != null) {
                return@addSnapshotListener
            }
            for (doc in snapshot!!.documentChanges) {
                when (doc.type) {
                    DocumentChange.Type.ADDED -> {

                            val player = Player(
                                doc.document.getString("name")!!,
                                doc.document.getDouble("score")!!.toInt()
                            )
                            players.add(player)
                            notifyItemInserted(players.size - 1)

                    }
                    else -> return@addSnapshotListener
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return players.size
    }

    override fun onBindViewHolder(holder: scoreViewHolder, position: Int) {

        holder.playerName.text = players[position].name
        holder.playerScore.text = players[position].score.toString()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): scoreViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.score_view_holder, parent, false)
        return scoreViewHolder(view)
    }


}
